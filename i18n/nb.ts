<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AccountSettingsList</name>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="26"/>
        <source>Account Settings</source>
        <translation type="unfinished">Kontoinnstillinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="56"/>
        <source>Details</source>
        <translation type="unfinished">Detaljer</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="60"/>
        <source>Incoming Server</source>
        <translation type="unfinished">Innkommende server</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="64"/>
        <source>Outgoing Server</source>
        <translation type="unfinished">Utgående Server</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="68"/>
        <source>Copies and Folders</source>
        <translation type="unfinished">Kopier og Mapper</translation>
    </message>
</context>
<context>
    <name>AddAnotherUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="30"/>
        <source>Success</source>
        <translation type="unfinished">Fullført</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="89"/>
        <source>New account created.</source>
        <translation type="unfinished">Ny konto er opprettet.</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="98"/>
        <source>Continue</source>
        <translation type="unfinished">Fortsett</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="113"/>
        <source>Add another</source>
        <translation type="unfinished">Legg til en annen</translation>
    </message>
</context>
<context>
    <name>AddressBookList</name>
    <message>
        <location filename="../plugins/core/mail/contacts/AddressBookList.qml" line="12"/>
        <source>Addressbooks</source>
        <translation type="unfinished">Adressebøker</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/AddressBookList.qml" line="65"/>
        <source>Add Collection</source>
        <translation type="unfinished">Legg til samling</translation>
    </message>
</context>
<context>
    <name>AddressBookStage</name>
    <message>
        <location filename="../plugins/core/contacts/AddressBookStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AttachmentPanel</name>
    <message>
        <location filename="../plugins/core/mail/components/AttachmentPanel.qml" line="81"/>
        <source>Attachments</source>
        <translation type="unfinished">Vedlegg</translation>
    </message>
</context>
<context>
    <name>AttachmentPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/AttachmentPopover.qml" line="46"/>
        <source>Delete</source>
        <translation type="unfinished">Slett</translation>
    </message>
</context>
<context>
    <name>AttachmentsStage</name>
    <message>
        <location filename="../plugins/extensions/attachments/AttachmentsStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AuthenticationSelector</name>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="45"/>
        <source>Authentication</source>
        <translation type="unfinished">Autentisering</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="60"/>
        <source>PLAIN</source>
        <translation type="unfinished">PLAIN</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="61"/>
        <source>LOGIN</source>
        <translation type="unfinished">LOGIN</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="62"/>
        <source>CRAM-MD5</source>
        <translation type="unfinished">CRAM-MD5</translation>
    </message>
</context>
<context>
    <name>AutoConfigState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="35"/>
        <source>Searching for configuration.</source>
        <translation type="unfinished">Søker etter konfigurasjoner.</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="77"/>
        <source>IMAP server found</source>
        <translation type="unfinished">IMAP tjener funnet</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="78"/>
        <source>A IMAP server configuration was found for your domain.

Would you like to use this instead?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BottomEdgeComposer</name>
    <message>
        <location filename="../plugins/core/mail/composer/BottomEdgeComposer.qml" line="65"/>
        <source>Attachments</source>
        <translation type="unfinished">Vedlegg</translation>
    </message>
</context>
<context>
    <name>CalendarStage</name>
    <message>
        <location filename="../plugins/core/calendar/CalendarStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComposeWindow</name>
    <message>
        <location filename="../plugins/core/mail/composer/ComposeWindow.qml" line="24"/>
        <source>Dekko Composer</source>
        <translation type="unfinished">Dekko Komponist</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../plugins/core/mail/composer/Composer.qml" line="53"/>
        <source>Attach</source>
        <translation type="unfinished">Legg ved</translation>
    </message>
</context>
<context>
    <name>ConfirmationDialog</name>
    <message>
        <location filename="../imports/dialogs/ConfirmationDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation type="unfinished">Avbryt</translation>
    </message>
    <message>
        <location filename="../imports/dialogs/ConfirmationDialog.qml" line="63"/>
        <source>Confirm</source>
        <translation type="unfinished">Bekreft</translation>
    </message>
</context>
<context>
    <name>ContactFilterView</name>
    <message>
        <location filename="../plugins/core/mail/views/ContactFilterView.qml" line="112"/>
        <source>Add contact</source>
        <translation type="unfinished">Legg til kontakt</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/ContactFilterView.qml" line="123"/>
        <source>Send message</source>
        <translation type="unfinished">Send melding</translation>
    </message>
</context>
<context>
    <name>ContactListPage</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactListPage.qml" line="11"/>
        <source>Address book</source>
        <translation type="unfinished">Adressebok</translation>
    </message>
</context>
<context>
    <name>ContactView</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="11"/>
        <source>Contact</source>
        <translation type="unfinished">Kontakt</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="61"/>
        <source>Email</source>
        <translation type="unfinished">E-post</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="78"/>
        <source>Phone</source>
        <translation type="unfinished">Telefon</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="92"/>
        <source>Address</source>
        <translation type="unfinished">Adresse</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="96"/>
        <source>Street</source>
        <translation type="unfinished">Gate</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="102"/>
        <source>City</source>
        <translation type="unfinished">By</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="108"/>
        <source>Zip</source>
        <translation type="unfinished">Zip</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="114"/>
        <source>Country</source>
        <translation type="unfinished">Land</translation>
    </message>
</context>
<context>
    <name>ContactsListView</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactsListView.qml" line="36"/>
        <source>Search</source>
        <translation type="unfinished">Søk</translation>
    </message>
</context>
<context>
    <name>ContentBlockedNotice</name>
    <message>
        <location filename="../plugins/core/mail/webview/ContentBlockedNotice.qml" line="41"/>
        <source>Remote content blocked</source>
        <translation type="unfinished">Blokker externt innhold</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/webview/ContentBlockedNotice.qml" line="54"/>
        <source>Allow</source>
        <translation type="unfinished">Tillatt</translation>
    </message>
</context>
<context>
    <name>ContributorsPage</name>
    <message>
        <location filename="../plugins/core/mail/views/ContributorsPage.qml" line="25"/>
        <source>Contributors</source>
        <translation type="unfinished">Bidragsytere</translation>
    </message>
</context>
<context>
    <name>CopyFoldersGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="30"/>
        <source>Copies and Folders</source>
        <translation type="unfinished">Kopier og Mapper</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="124"/>
        <source>Standard folders</source>
        <translation type="unfinished">Standard Mapper</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="136"/>
        <source>Detect standard folders</source>
        <translation type="unfinished">Oppdag standarmapper</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="152"/>
        <source>Detect</source>
        <translation type="unfinished">Oppdag</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="173"/>
        <source>Base folder</source>
        <translation type="unfinished">Hoved mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="175"/>
        <source>Leave empty if you are unsure</source>
        <translation type="unfinished">La stå tom hvis usikker</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="190"/>
        <source>Inbox folder</source>
        <translation type="unfinished">Inboks mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="205"/>
        <source>Drafts folder</source>
        <translation type="unfinished">Kladd mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="220"/>
        <source>Spam folder</source>
        <translation type="unfinished">Søpperpost mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="235"/>
        <source>Sent folder</source>
        <translation type="unfinished">Sendt mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="250"/>
        <source>Outbox folder</source>
        <translation type="unfinished">Utboks mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="265"/>
        <source>Trash folder</source>
        <translation type="unfinished">Papirkurv mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="274"/>
        <source>Sending messages</source>
        <translation type="unfinished">Sender meldinger</translation>
    </message>
</context>
<context>
    <name>DefaultMessagePage</name>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="182"/>
        <source>From:</source>
        <translation type="unfinished">Fra:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="235"/>
        <source>To:</source>
        <translation type="unfinished">Til:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="240"/>
        <source>Cc:</source>
        <translation type="unfinished">Kopi:</translation>
    </message>
</context>
<context>
    <name>DefaultPlugin</name>
    <message>
        <location filename="../plugins/extensions/addressbook/DefaultPlugin.qml" line="11"/>
        <source>Internal</source>
        <translation type="unfinished">Intern</translation>
    </message>
    <message>
        <location filename="../plugins/extensions/addressbook/DefaultPlugin.qml" line="106"/>
        <source>Default</source>
        <translation type="unfinished">Standard</translation>
    </message>
</context>
<context>
    <name>DekkoHeader</name>
    <message>
        <location filename="../imports/components/DekkoHeader.qml" line="204"/>
        <source>Enter search...</source>
        <translation type="unfinished">Skriv inn søk …</translation>
    </message>
</context>
<context>
    <name>DekkoWebView</name>
    <message>
        <location filename="../plugins/core/mail/webview/DekkoWebView.qml" line="142"/>
        <source>Open in browser?</source>
        <translation type="unfinished">Vil du åpne dette i en nettleser?</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/webview/DekkoWebView.qml" line="143"/>
        <source>Confirm to open %1 in web browser</source>
        <translation type="unfinished">Bekreft åpning av %1 i nettleser</translation>
    </message>
</context>
<context>
    <name>DetailList</name>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="47"/>
        <source>Details</source>
        <translation type="unfinished">Detaljer</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="52"/>
        <source>To:</source>
        <translation type="unfinished">Til:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="58"/>
        <source>Cc:</source>
        <translation type="unfinished">Kopi:</translation>
    </message>
</context>
<context>
    <name>DetailsGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/DetailsGroup.qml" line="27"/>
        <source>Details</source>
        <translation type="unfinished">Detaljer</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DetailsGroup.qml" line="62"/>
        <source>Account name</source>
        <translation type="unfinished">Kontonavn</translation>
    </message>
</context>
<context>
    <name>DisplaySettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="28"/>
        <source>Navigation menu</source>
        <translation type="unfinished">Navigasjons meny</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="32"/>
        <source>Show smart folders</source>
        <translation type="unfinished">Vis Smart mapper</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="42"/>
        <source>Show favourite folders</source>
        <translation type="unfinished">Vis favoritt mapper</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="53"/>
        <source>Messages</source>
        <translation type="unfinished">Meldinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="57"/>
        <source>Show avatars</source>
        <translation type="unfinished">Vis personbilder</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="67"/>
        <source>Prefer plain text</source>
        <translation type="unfinished">Foretrekker simpel tekst</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettingsPage.qml" line="21"/>
        <source>Display Settings</source>
        <translation type="unfinished">Skjerminnstillinger</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPopup</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettingsPopup.qml" line="21"/>
        <source>Display Settings</source>
        <translation type="unfinished">Skjerminnstillinger</translation>
    </message>
</context>
<context>
    <name>EncryptionSelector</name>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="46"/>
        <source>Encryption</source>
        <translation type="unfinished">Kryptering</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="61"/>
        <source>No encryption</source>
        <translation type="unfinished">Ingen kryptering</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="62"/>
        <source>Use encryption (STARTTLS)</source>
        <translation type="unfinished">Bruk kryptering (STARTTLS)</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="63"/>
        <source>Force encryption (SSL/TLS)</source>
        <translation type="unfinished">Tving kryptering (SSL/TLS)</translation>
    </message>
</context>
<context>
    <name>ExpandablePanel</name>
    <message>
        <location filename="../imports/components/ExpandablePanel.qml" line="62"/>
        <source>Attachments</source>
        <translation type="unfinished">Vedlegg</translation>
    </message>
</context>
<context>
    <name>FilePickerDialog</name>
    <message>
        <location filename="../imports/dialogs/FilePickerDialog.qml" line="23"/>
        <source>Add Attachment</source>
        <translation type="unfinished">Legg til vedlegg</translation>
    </message>
</context>
<context>
    <name>FolderListDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/FolderListDelegate.qml" line="52"/>
        <source>Un-favourite</source>
        <translation type="unfinished">Avfavorittisere</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/FolderListDelegate.qml" line="52"/>
        <source>Favourite</source>
        <translation type="unfinished">Favoritter</translation>
    </message>
</context>
<context>
    <name>HtmlViewer</name>
    <message>
        <location filename="../plugins/extensions/html-viewer/HtmlViewer.qml" line="11"/>
        <source>HTML Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IdentitiesListPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/IdentitiesListPage.qml" line="14"/>
        <source>Identities</source>
        <translation type="unfinished">Identiteter</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentitiesListPage.qml" line="106"/>
        <source> (Default)</source>
        <translation type="unfinished"> (Standard)</translation>
    </message>
</context>
<context>
    <name>IdentityInput</name>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="54"/>
        <source>Default identity</source>
        <translation type="unfinished">Standard identitet</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="67"/>
        <source>Account</source>
        <translation type="unfinished">Konto</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="105"/>
        <source>Name</source>
        <translation type="unfinished">Navn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="110"/>
        <source>Email Address</source>
        <translation type="unfinished">E-postadresse</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="115"/>
        <source>Reply-To</source>
        <translation type="unfinished">Svar til</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="119"/>
        <source>Signature</source>
        <translation type="unfinished">Signatur</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="137"/>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="145"/>
        <source>New identity</source>
        <translation type="unfinished">Ny identitet</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="169"/>
        <source>Edit identity</source>
        <translation type="unfinished">Rediger identitet</translation>
    </message>
</context>
<context>
    <name>IncomingServerGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="28"/>
        <source>Incoming Server</source>
        <translation type="unfinished">Innkommende server</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="105"/>
        <source>Hostname</source>
        <translation type="unfinished">Vertsnavn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="114"/>
        <source>Port</source>
        <translation type="unfinished">Port</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="123"/>
        <source>Username</source>
        <translation type="unfinished">Brukernavn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="132"/>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="135"/>
        <source>Password</source>
        <translation type="unfinished">Passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="142"/>
        <source>Show password</source>
        <translation type="unfinished">Vis passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="148"/>
        <source>Security settings</source>
        <translation type="unfinished">Sikkerhetsinnstillinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="183"/>
        <source>Allow untrusted certificates</source>
        <translation type="unfinished">Tillat ukjente sertifikater</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="188"/>
        <source>Server settings</source>
        <translation type="unfinished">Tjenerinnstillinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="193"/>
        <source>Check for new mail on start</source>
        <translation type="unfinished">Se etter nye eposter ved start</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="199"/>
        <source>Enable IMAP IDLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="206"/>
        <source>Check interval (minutes)</source>
        <translation type="unfinished">sjekk interval (minutter)</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="213"/>
        <source>Check when roaming</source>
        <translation type="unfinished">Sjekk med roaming</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="219"/>
        <source>Maximum mail size (MB)</source>
        <translation type="unfinished">Maks epost størelse</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="225"/>
        <source>No maximum mail size</source>
        <translation type="unfinished">Ingen maksimum epost størelse</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="233"/>
        <source>Automatically download attachments</source>
        <translation type="unfinished">Last ned vedlegg automatisk</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="239"/>
        <source>Allowed to delete mail</source>
        <translation type="unfinished">Godkjent til og slette epost</translation>
    </message>
</context>
<context>
    <name>LicensesPage</name>
    <message>
        <location filename="../plugins/core/mail/views/LicensesPage.qml" line="25"/>
        <source>Licenses</source>
        <translation type="unfinished">Lisenser</translation>
    </message>
</context>
<context>
    <name>MailSettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="11"/>
        <source>Mail Settings</source>
        <translation type="unfinished">Epost Instillinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="24"/>
        <source>Accounts</source>
        <translation type="unfinished">Kontoer</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="34"/>
        <source>Identities</source>
        <translation type="unfinished">Identiteter</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="44"/>
        <source>Display</source>
        <translation type="unfinished">Skjerm</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="51"/>
        <source>Privacy</source>
        <translation type="unfinished">Personvern</translation>
    </message>
</context>
<context>
    <name>MailSettingsAction</name>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettingsAction.qml" line="7"/>
        <source>Mail</source>
        <translation type="unfinished">Epost</translation>
    </message>
</context>
<context>
    <name>MailUtils</name>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="27"/>
        <source>To</source>
        <translation type="unfinished">Til</translation>
    </message>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="29"/>
        <source>Cc</source>
        <translation type="unfinished">Kopi</translation>
    </message>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="31"/>
        <source>Bcc</source>
        <translation type="unfinished">Blindkopi</translation>
    </message>
</context>
<context>
    <name>MailboxPickerPage</name>
    <message>
        <location filename="../plugins/core/mail/views/MailboxPickerPage.qml" line="34"/>
        <source>Select folder</source>
        <translation type="unfinished">Velg mappe</translation>
    </message>
</context>
<context>
    <name>MainUI</name>
    <message>
        <location filename="../qml/MainUI.qml" line="17"/>
        <source>Dekko Mail</source>
        <translation type="unfinished">Dekko Epost</translation>
    </message>
</context>
<context>
    <name>ManageAccountsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/ManageAccountsPage.qml" line="26"/>
        <source>Manage accounts</source>
        <translation type="unfinished">Administrer kontakter</translation>
    </message>
</context>
<context>
    <name>ManualInputUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="28"/>
        <source>Server configuration</source>
        <translation type="unfinished">Serverkonfigurasjon</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="33"/>
        <source>IMAP Server:</source>
        <translation type="unfinished">IMAP-tjener:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="46"/>
        <source>POP3 Server:</source>
        <translation type="unfinished">POP3 Tjener:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="59"/>
        <source>SMTP Server:</source>
        <translation type="unfinished">SMTP Tjener:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="71"/>
        <source>Back</source>
        <translation type="unfinished">Tilbake</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="75"/>
        <source>Next</source>
        <translation type="unfinished">Neste</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Password empty</source>
        <translation type="unfinished">Passord er tomt</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Would you like to continue?</source>
        <translation type="unfinished">Vil du fortsette?</translation>
    </message>
</context>
<context>
    <name>MarkdownEditor</name>
    <message>
        <location filename="../plugins/extensions/Markdown/MarkdownEditor.qml" line="66"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageActionPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="44"/>
        <source>Reply all</source>
        <translation type="unfinished">Svar alle</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="52"/>
        <source>Forward</source>
        <translation type="unfinished">Videresend</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="65"/>
        <source>Move</source>
        <translation type="unfinished">Flytt</translation>
    </message>
</context>
<context>
    <name>MessageHeader</name>
    <message>
        <location filename="../plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>Hide details</source>
        <translation type="unfinished">Skjul detaljer</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>View details</source>
        <translation type="unfinished">Vis detaljer</translation>
    </message>
</context>
<context>
    <name>MessageListActionPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="44"/>
        <source>Mark as unread</source>
        <translation type="unfinished">Marker som ulest</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="44"/>
        <source>Mark as read</source>
        <translation type="unfinished">Merk som lest</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="52"/>
        <source>Mark as not important</source>
        <translation type="unfinished">Merk som ikke viktig</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="52"/>
        <source>Mark as important</source>
        <translation type="unfinished">Merk som viktig</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="60"/>
        <source>Mark as spam</source>
        <translation type="unfinished">Merk som søpperpost</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="73"/>
        <source>To-do</source>
        <translation type="unfinished">Gjøremål</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="89"/>
        <source>Done</source>
        <translation type="unfinished">Ferdig</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="103"/>
        <source>Reply</source>
        <translation type="unfinished">Svar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="111"/>
        <source>Reply all</source>
        <translation type="unfinished">Svar alle</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="119"/>
        <source>Forward</source>
        <translation type="unfinished">Videresend</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="133"/>
        <source>Move</source>
        <translation type="unfinished">Flytt</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="142"/>
        <source>Restore to %1</source>
        <translation type="unfinished">Gjennopprett til %1</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="151"/>
        <source>Delete</source>
        <translation type="unfinished">Slett</translation>
    </message>
</context>
<context>
    <name>MessageListDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="50"/>
        <source>Un-mark flagged</source>
        <translation type="unfinished">avmerk flagget</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="50"/>
        <source>Mark flagged</source>
        <translation type="unfinished">merk flagget</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="59"/>
        <source>Mark as un-read</source>
        <translation type="unfinished">Merk som ulest</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="59"/>
        <source>Mark as read</source>
        <translation type="unfinished">Merk som lest</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="66"/>
        <source>Move message</source>
        <translation type="unfinished">Flytt melding</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="71"/>
        <source>Context menu</source>
        <translation type="unfinished">Meny</translation>
    </message>
</context>
<context>
    <name>MessageListView</name>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="114"/>
        <source>Unselect all</source>
        <translation type="unfinished">Velg bort alle</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="114"/>
        <source>Select all</source>
        <translation type="unfinished">Velg alle</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="126"/>
        <source>Star</source>
        <translation type="unfinished">Gi stjerne</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="126"/>
        <source>Remove star</source>
        <translation type="unfinished">Fjern stjerne</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="130"/>
        <source>Mark as un-read</source>
        <translation type="unfinished">Merk som ulest</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="130"/>
        <source>Mark as read</source>
        <translation type="unfinished">Merk som lest</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="137"/>
        <source>Delete</source>
        <translation type="unfinished">Slett</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="332"/>
        <source>Load more messages ...</source>
        <translation type="unfinished">Last inn flere meldinger ...</translation>
    </message>
</context>
<context>
    <name>MessageViewContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="46"/>
        <source>Open in browser</source>
        <translation type="unfinished">Åpne i nettleser</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="54"/>
        <source>Copy link</source>
        <translation type="unfinished">Kopier lenke</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="63"/>
        <source>Share link</source>
        <translation type="unfinished">Del lenke</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="77"/>
        <source>Reply</source>
        <translation type="unfinished">Svar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="86"/>
        <source>Reply all</source>
        <translation type="unfinished">Svar alle</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="94"/>
        <source>Forward</source>
        <translation type="unfinished">Videresend</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="106"/>
        <source>View source</source>
        <translation type="unfinished">Vis kildekode</translation>
    </message>
</context>
<context>
    <name>NavMenuAccountSettingsModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="29"/>
        <source>Manage accounts</source>
        <translation type="unfinished">Administrer kontakter</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="45"/>
        <source>Display settings</source>
        <translation type="unfinished">Skjerminnstillinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="67"/>
        <source>Privacy settings</source>
        <translation type="unfinished">Personverninnstillinger</translation>
    </message>
</context>
<context>
    <name>NavMenuContactsModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="27"/>
        <source>Addressbook</source>
        <translation type="unfinished">Adressebok</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="43"/>
        <source>Recent contacts</source>
        <translation type="unfinished">Nylig brukte kontakter</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="58"/>
        <source>Import contacts</source>
        <translation type="unfinished">Importer kontakter</translation>
    </message>
</context>
<context>
    <name>NavMenuDekkoVisualModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="27"/>
        <source>Version</source>
        <translation type="unfinished">Versjon</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="45"/>
        <source>Licenses</source>
        <translation type="unfinished">Lisenser</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="63"/>
        <source>Contributors</source>
        <translation type="unfinished">Bidragsytere</translation>
    </message>
</context>
<context>
    <name>NavMenuModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuModel.qml" line="98"/>
        <source>Smart folders</source>
        <translation type="unfinished">Smarte mapper</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuModel.qml" line="120"/>
        <source>Folders</source>
        <translation type="unfinished">Mapper</translation>
    </message>
</context>
<context>
    <name>NavMenuPage</name>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="30"/>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="64"/>
        <source>Mail</source>
        <translation type="unfinished">Epost</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="67"/>
        <source>Contacts</source>
        <translation type="unfinished">Kontakter</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="70"/>
        <source>Settings</source>
        <translation type="unfinished">Innstillinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="73"/>
        <source>About</source>
        <translation type="unfinished">Om</translation>
    </message>
</context>
<context>
    <name>NavMenuStandardFolderDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/NavMenuStandardFolderDelegate.qml" line="192"/>
        <source>Inbox (%1)</source>
        <translation type="unfinished">Innboks (%1)</translation>
    </message>
</context>
<context>
    <name>NavSideBar</name>
    <message>
        <location filename="../imports/components/private/NavSideBar.qml" line="155"/>
        <location filename="../plugins/core/mail/views/NavSideBar.qml" line="155"/>
        <source>Smart folders</source>
        <translation type="unfinished">Smarte mapper</translation>
    </message>
    <message>
        <location filename="../imports/components/private/NavSideBar.qml" line="169"/>
        <location filename="../plugins/core/mail/views/NavSideBar.qml" line="169"/>
        <source>Folders</source>
        <translation type="unfinished">Mapper</translation>
    </message>
</context>
<context>
    <name>NavViewContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="44"/>
        <source>Sync folder</source>
        <translation type="unfinished">Synkroniser mappe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="53"/>
        <source>Send pending</source>
        <translation type="unfinished">Send ventende</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="70"/>
        <source>Mark folder read</source>
        <translation type="unfinished">Merk mappe som lest</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="79"/>
        <source>Mark all done</source>
        <translation type="unfinished">Merk alle ferdig</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="101"/>
        <source>Empty trash</source>
        <translation type="unfinished">Tøm papirkurv</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="110"/>
        <source>Folder properties</source>
        <translation type="unfinished">Egenskaper for mappe</translation>
    </message>
</context>
<context>
    <name>NewAccountsUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NewAccountsUI.qml" line="32"/>
        <source>New account</source>
        <translation type="unfinished">Ny konto</translation>
    </message>
</context>
<context>
    <name>NoAccountsUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="30"/>
        <source>Accounts</source>
        <translation type="unfinished">Kontoer</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="92"/>
        <source>No email account is setup.</source>
        <translation type="unfinished">Ingen epost konto er satt opp.</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="101"/>
        <source>Add now</source>
        <translation type="unfinished">Legg til nå</translation>
    </message>
</context>
<context>
    <name>NotesStage</name>
    <message>
        <location filename="../plugins/core/notes/NotesStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NothingSelectedPage</name>
    <message>
        <location filename="../plugins/core/mail/views/NothingSelectedPage.qml" line="55"/>
        <source>No message selected</source>
        <translation type="unfinished">Ingen melding valgt</translation>
    </message>
</context>
<context>
    <name>OutgoingServerGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="28"/>
        <source>Outgoing Server</source>
        <translation type="unfinished">Utgående Server</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="75"/>
        <source>Hostname</source>
        <translation type="unfinished">Vertsnavn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="83"/>
        <source>Port</source>
        <translation type="unfinished">Port</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="92"/>
        <source>Username</source>
        <translation type="unfinished">Brukernavn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="100"/>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="103"/>
        <source>Password</source>
        <translation type="unfinished">Passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="110"/>
        <source>Show password</source>
        <translation type="unfinished">Vis passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="116"/>
        <source>Security settings</source>
        <translation type="unfinished">Sikkerhetsinnstillinger</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="149"/>
        <source>Authenticate from server capabilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="155"/>
        <source>Allow untrusted certificates</source>
        <translation type="unfinished">Tillat ukjente sertifikater</translation>
    </message>
</context>
<context>
    <name>PrivacySettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="28"/>
        <source>Message content</source>
        <translation type="unfinished">Meldingsinnhold</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="32"/>
        <source>Allow remote content</source>
        <translation type="unfinished">Godta eksternt innhold</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="42"/>
        <source>Auto load images</source>
        <translation type="unfinished">Last inn bilder automatisk</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettingsPage.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation type="unfinished">Personverninnstillinger</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPopup</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettingsPopup.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation type="unfinished">Personverninnstillinger</translation>
    </message>
</context>
<context>
    <name>RecipientField</name>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientField.qml" line="97"/>
        <source>Enter an address</source>
        <translation type="unfinished">Skriv inn adresse</translation>
    </message>
</context>
<context>
    <name>RecipientInfo</name>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="41"/>
        <source>Back</source>
        <translation type="unfinished">Tilbake</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="93"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Kopier til utklippstavlen</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="107"/>
        <source>Add to addressbook</source>
        <translation type="unfinished">Legg til i adressebok</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="122"/>
        <source>Send message</source>
        <translation type="unfinished">Send melding</translation>
    </message>
</context>
<context>
    <name>RecipientInputContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="55"/>
        <source>Add CC</source>
        <translation type="unfinished">Legg til kopi</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="68"/>
        <source>Add BCC</source>
        <translation type="unfinished">Legg til blindkopi</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="80"/>
        <source>Add contact</source>
        <translation type="unfinished">Legg til kontakt</translation>
    </message>
</context>
<context>
    <name>RecipientPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="80"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Kopier til utklippstavlen</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="89"/>
        <source>Add to addressbook</source>
        <translation type="unfinished">Legg til i adressebok</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="98"/>
        <source>Send message</source>
        <translation type="unfinished">Send melding</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="108"/>
        <source>Remove</source>
        <translation type="unfinished">Fjern</translation>
    </message>
</context>
<context>
    <name>SenderIdentityField</name>
    <message>
        <location filename="../plugins/core/mail/composer/SenderIdentityField.qml" line="53"/>
        <source>From:</source>
        <translation type="unfinished">Fra:</translation>
    </message>
</context>
<context>
    <name>ServerDetails</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="69"/>
        <source>Hostname</source>
        <translation type="unfinished">Vertsnavn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="78"/>
        <source>Port</source>
        <translation type="unfinished">Port</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="100"/>
        <source>Username</source>
        <translation type="unfinished">Brukernavn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="109"/>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="112"/>
        <source>Password</source>
        <translation type="unfinished">Passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="119"/>
        <source>Show password</source>
        <translation type="unfinished">Vis passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="140"/>
        <source>Allow untrusted certificates</source>
        <translation type="unfinished">Tillat ukjente sertifikater</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../plugins/core/settings/Settings.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished">Innstillinger</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../plugins/core/settings/SettingsWindow.qml" line="26"/>
        <source>Dekko Settings</source>
        <translation type="unfinished">Dekko Instillinger</translation>
    </message>
</context>
<context>
    <name>SetupWizardWindow</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/SetupWizardWindow.qml" line="20"/>
        <source>Mail Setup Wizard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmartFolderDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/SmartFolderDelegate.qml" line="155"/>
        <source>Inbox (%1)</source>
        <translation type="unfinished">Innboks (%1)</translation>
    </message>
</context>
<context>
    <name>SubjectField</name>
    <message>
        <location filename="../plugins/core/mail/composer/SubjectField.qml" line="56"/>
        <source>Subject:</source>
        <translation type="unfinished">Emne:</translation>
    </message>
</context>
<context>
    <name>SyncState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/SyncState.qml" line="35"/>
        <source>Synchronizing account.</source>
        <translation type="unfinished">Synkroniserer konto.</translation>
    </message>
</context>
<context>
    <name>TitledTextField</name>
    <message>
        <location filename="../imports/components/TitledTextField.qml" line="61"/>
        <source> (Required)</source>
        <translation type="unfinished"> (Påkrevet)</translation>
    </message>
</context>
<context>
    <name>UserInputUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="35"/>
        <source>Name</source>
        <translation type="unfinished">Navn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="36"/>
        <source>Full name</source>
        <translation type="unfinished">Fullt navn</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="43"/>
        <source>Description</source>
        <translation type="unfinished">Beskrivelse</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="44"/>
        <source>E.g Home, Work...</source>
        <translation type="unfinished">Eks. Hjemme, Jobb...</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="51"/>
        <source>Email address</source>
        <translation type="unfinished">E-postadresse</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="53"/>
        <source>email@example.org</source>
        <translation type="unfinished">epost@eksempel.no</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="59"/>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="62"/>
        <source>Password</source>
        <translation type="unfinished">Passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="68"/>
        <source>Show password</source>
        <translation type="unfinished">Vis passord</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="78"/>
        <source>Cancel</source>
        <translation type="unfinished">Avbryt</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="82"/>
        <source>Next</source>
        <translation type="unfinished">Neste</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="124"/>
        <source>Password empty</source>
        <translation type="unfinished">Passord er tomt</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="124"/>
        <source>Would you like to continue?</source>
        <translation type="unfinished">Vil du fortsette?</translation>
    </message>
</context>
<context>
    <name>ValidationState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/ValidationState.qml" line="36"/>
        <source>Validating credentials.</source>
        <translation type="unfinished">Validerer legitimasjon</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../imports/dialogs/VersionDialog.qml" line="25"/>
        <source>Version</source>
        <translation type="unfinished">Versjon</translation>
    </message>
    <message>
        <location filename="../imports/dialogs/VersionDialog.qml" line="29"/>
        <source>Close</source>
        <translation type="unfinished">Lukk</translation>
    </message>
</context>
</TS>
